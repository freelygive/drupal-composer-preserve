# Drupal Composer Preserve

This composer plugin will prevent Drupal modules and themes from being 
uninstalled while they are still enabled in the Drupal system.

## Usage
`composer require freelygive/drupal-composer-preserve`

The plugin has to be installed in the environment before it will have an 
effect, so you can't deploy at the same time as a package removal.
