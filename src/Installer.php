<?php

namespace FreelyGive\DrupalComposerPreserve;

use Composer\Installers\Installer as BaseInstaller;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;
use cweagans\Composer\Patches;

/**
 * Installer that will preserve installed Drupal packages.
 *
 * @package FreelyGive\DrupalComposerPreserve
 */
class Installer extends BaseInstaller {

  /**
   * The IO handler.
   *
   * @var \Composer\IO\IOInterface
   */
  protected $io;

  /**
   * The installed Drupal modules and themes, keyed by path.
   *
   * @var array
   */
  protected $installedPackages;

  /**
   * Initialise the installed modules and themes.
   */
  protected function initInstalledPackages() : void {
    if (isset($this->installedPackages)) {
      return;
    }

    $this->installedPackages = [];

    // Use drush to get the enabled modules.
    $result_string = shell_exec('./vendor/bin/drush pm:list --status=enabled --no-core --fields=name --format=json 2>/dev/null');
    if ($result_string) {
      $result = json_decode($result_string, TRUE);
      if ($result) {
        $this->installedPackages = array_keys($result);
      }
    }
  }

  /**
   * Check whether a package is installed.
   *
   * @param \Composer\Package\PackageInterface $package
   *   The package to check.
   *
   * @return bool
   *   Whether the package is enabled.
   */
  protected function isPackageInstalled(PackageInterface $package) : bool {
    $this->initInstalledPackages();
    $drupal_name = explode('/', $package->getName(), 2)[1];
    return in_array($drupal_name, $this->installedPackages);
  }

  /**
   * Check whether we should allow removal for re-patching.
   *
   * @param \Composer\Package\PackageInterface $package
   *   The package to check.
   *
   * @return bool
   *   Whether to allow the removal.
   */
  protected function allowRemovalForPatches(PackageInterface $package) : bool {
    // The only way to know if this is a patches removal is to check the
    // backtrace.
    $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 4);
    $call = end($backtrace);
    if ($call['class'] != Patches::class || $call['function'] != 'checkPatches') {
      return FALSE;
    }

    // We need to be careful about packages that wont get re-installed and
    // prevent them from being removed. To do this we need to check the lock.
    $locked_repo = $this->composer->getLocker()->getLockedRepository();
    return $locked_repo->hasPackage($package);
  }

  /**
   * {@inheritDoc}
   */
  public function supports($packageType) {
    return in_array($packageType, ['drupal-module', 'drupal-theme']);
  }

  /**
   * {@inheritDoc}
   */
  public function uninstall(InstalledRepositoryInterface $repo, PackageInterface $package) {
    // Don't remove packages still installed in Drupal.
    if ($this->isPackageInstalled($package)) {
      // The only exception to this is we want to allow packages to be removed
      // for re-patching.
      if (!$this->allowRemovalForPatches($package)) {
        $this->io->write("<info>Skipping removal of enabled module {$package->getPrettyName()} [{$package->getName()}].</info>");
        return;
      }
    }

    parent::uninstall($repo, $package);
  }

}
