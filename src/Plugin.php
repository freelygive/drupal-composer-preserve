<?php

namespace FreelyGive\DrupalComposerPreserve;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

/**
 * Plugin for preserving installed Drupal packages.
 *
 * @package FreelyGive\DrupalComposerPreserve
 */
class Plugin implements PluginInterface {

  /**
   * {@inheritdoc}
   */
  public function activate(Composer $composer, IOInterface $io) {
    $installer = new Installer($io, $composer);
    $composer->getInstallationManager()->addInstaller($installer);
  }

}
